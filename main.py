import os
import uuid

from pydantic import BaseModel

import openai
import requests
from fastapi import FastAPI, File, Request, UploadFile
from fastapi.responses import FileResponse, HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from llm import LanguageModel
from qdrant import LocalQdrant, extract_context, unpack_results
from utils import Config

config = Config("config.json")

client = LocalQdrant(
    url=config.qdrant_url,
    qdrant_api_key=config.qdrant_api_key,
    collection_name=config.collection_name,
    openai_api_key=config.openai_api_key,
)

language_model = LanguageModel(
    openai_api_key=config.openai_api_key,
    model=config.model,
    temperature=config.temperature,
)

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")


# Add your OpenAI API key
OPENAI_API_KEY = config.openai_api_key
# openai.api_key = OPENAI_API_KEY

# Add your ElevenLabs API key
ELEVENLABS_API_KEY = config.elevenlabs_api_key
ELEVENLABS_VOICE_STABILITY = 0.30
ELEVENLABS_VOICE_SIMILARITY = 0.75

# Choose your favorite ElevenLabs voice
ELEVENLABS_VOICE_NAME = "male_voice_one"
ELEVENLABS_ALL_VOICES = []


def retrieve_voices() -> list:
    """Retrieve all voices from ElevenLabs API

    Returns
    -------
    list
        List of all voices
    """
    url = "https://api.elevenlabs.io/v1/voices"
    headers = {"xi-api-key": ELEVENLABS_API_KEY}
    response = requests.get(url, headers=headers)
    return response.json()["voices"]


def transcribe_audio(filename: str) -> str:
    """Transcribe audio file using OpenAI's Whisper API

    Parameters
    ----------
    filename : str
        FIlename of the audio file to be transscribed

    Returns
    -------
    str
        Transciption of the audio file
    """
    with open(filename, "rb") as audio_file:
        transcript = openai.Audio.transcribe("whisper-1", audio_file, OPENAI_API_KEY)
    return transcript.text


def generate_reply(conversation: list) -> str:
    """Generate a reply using Rasa's REST API

    Parameters
    ----------
    conversation : list
        The conversation history

    Returns
    -------
    str
        The generated reply from the Rasa chatbot
    """
    message = conversation[-1]["content"]
    try: 
        search_results = client.search(query=message)
        if len(search_results) == 0:
            prompt = f"HUMAN: {message}"
        else:
            results = unpack_results(search_results)

            context = extract_context(
                results=results, token_limit=config.token_limit
            )

            prompt = f"CONTEXT: {context} \n \n HUMAN: {message}"
            
        return language_model.generate(human_input=prompt)
    except Exception as e:
        # Hi Francis learn how to handle exceptions from openai and qdrant each should show a message the 
        # person in charge can understand
        return "Determined Worker"


def generate_audio(
    text: str,
    voice: str,
    output_path: str = "",
) -> str:
    """Generate audio file using ElevenLabs' REST API

    Parameters
    ----------
    text : str
        The openai's reply
    output_path : str, optional
        Output path of the generated audio file, by default ""

    Returns
    -------
    str
        Output path of the generated audio file
    """
    voices = ELEVENLABS_ALL_VOICES
    try:
        voice_id = next(filter(lambda v: v["name"] == ELEVENLABS_VOICE_NAME, voices))[
            "voice_id"
        ]
    except StopIteration:
        voice_id = voices[0]["voice_id"]

    if voice == "female":
        voice_id = "zjzzthCUahTDME25eySi"  # "utkv7t9zjzXaUIYBbCVp"
    else:
        voice_id = "aZe5y2s0tWqfxnqDqdwg"

    url = f"https://api.elevenlabs.io/v1/text-to-speech/{voice_id}"
    headers = {"xi-api-key": ELEVENLABS_API_KEY, "content-type": "application/json"}
    data = {
        "text": text,
        "voice_settings": {
            "stability": ELEVENLABS_VOICE_STABILITY,
            "similarity_boost": ELEVENLABS_VOICE_SIMILARITY,
        },
    }
    response = requests.post(url, json=data, headers=headers)
    with open(output_path, "wb") as output:
        output.write(response.content)
    return output_path


class TranscriptionResponse(BaseModel):
    text: str

class VoiceRequest(BaseModel):
    conversation: list
    voice: str

@app.get("/")
def read_root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})

@app.post("/transcribe")
def transcribe(file: UploadFile):
    if not file:
        return "No file found", 400

    recording_file = f"{uuid.uuid4()}.wav"
    recording_path = f"uploads/{recording_file}"
    os.makedirs(os.path.dirname(recording_path), exist_ok=True)

    with open(recording_path, "wb") as audio_file:
        audio_file.write(file.file.read())

    transcription = transcribe_audio(recording_path)
    return {"text": transcription}

@app.post("/ask")
def ask(data: VoiceRequest):
    conversation = data.conversation
    voice = data.voice
    reply = generate_reply(conversation)
    reply_file = f"{uuid.uuid4()}.mp3"
    reply_path = f"outputs/{reply_file}"
    os.makedirs(os.path.dirname(reply_path), exist_ok=True)
    generate_audio(reply, voice=voice, output_path=reply_path)
    return {"text": reply, "audio": f"/listen/{reply_file}"}

@app.get("/listen/{filename}")
def listen(filename: str):
    return FileResponse(f"outputs/{filename}", media_type="audio/mp3")


if ELEVENLABS_API_KEY:
    if not ELEVENLABS_ALL_VOICES:
        ELEVENLABS_ALL_VOICES = retrieve_voices()
    if not ELEVENLABS_VOICE_NAME:
        ELEVENLABS_VOICE_NAME = ELEVENLABS_ALL_VOICES[0]["name"]
